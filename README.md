# Kernel EOL Versions

As there is no official listing for eol kernel versions, you can find them all here, starting from 3.0

## Installation

- Run `git submodule add https://gitlab.com/reggermont/kernel-eol-versions`

## How to use

Depends on your project. JSON recommanded otherwise. Import it like any JSON file.

## License

This project is under [Apache 2.0](LICENSE) License